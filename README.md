# Codility Lesson
## Maximum Slice Problem - MaxSliceSum

Solution scores 100% and is in C#
[Lesson](https://app.codility.com/programmers/lessons/9-maximum_slice_problem/max_slice_sum/)

### Task description

A non-empty array A consisting of N integers is given. A pair of integers (P, Q), such that 0 ≤ P ≤ Q < N, is called a slice of array A. The sum of a slice (P, Q) is the total of A[P] + A[P+1] + ... + A[Q].

Write a function:

```cs
class Solution { public int solution(int[] A); }
```

that, given an array A consisting of N integers, returns the maximum sum of any slice of A.

For example, given array A such that:
```
A[0] = 3  A[1] = 2  A[2] = -6
A[3] = 4  A[4] = 0
```

the function should return 5 because:

(3, 4) is a slice of A that has sum 4,
(2, 2) is a slice of A that has sum −6,
(0, 1) is a slice of A that has sum 5,
no other slice of A has sum greater than (0, 1).
Write an efficient algorithm for the following assumptions:

- N is an integer within the range [1..1,000,000];
- each element of array A is an integer within the range [−1,000,000..1,000,000];
- the result will be an integer within the range [−2,147,483,648..2,147,483,647]

### Solution

```cs 
using System;
using System.Linq;

class Solution {

    public int solution(int[] A) {

        int maxEnd = 0;
        int maxSlice = 0;
        int maxA = A.Max();
       
        if (maxA > 0) {
        
            for(int i = 0; i < A.Length; i++) {
                maxEnd = Math.Max(0, maxEnd + A[i]);
                maxSlice = Math.Max(maxSlice, maxEnd);
            }
        } else {
            maxSlice = maxA;
        }
       
        return maxSlice;
    }
}
```
 
